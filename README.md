# Commerce Payin-Payout

This module integrates [Payin-Payout](https://payin-payout.net/) with
Drupal Commerce 2 providing payment gateway.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/commerce_payin_payout).

To submit bug reports and feature suggestions, or to track changes
[issue queue](https://www.drupal.org/project/issues/commerce_payin_payout).


## Contents of this file

- Requirements
- Installation
- Configuration
- How it works
- Troubleshooting
- Maintainers


## Requirements

This module requires the following:
- Submodules of [Drupal Commerce package](https://drupal.org/project/commerce)
    - [Commerce core](https://drupal.org/project/commerce)
    - Commerce Payment (and its dependencies)


## Installation

This module can to be installed via Composer or manually.

`composer require drupal/commerce_payin_payout`

OR

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Create a new Payin-Payout payment gateway.
- Select payment mode, add API token and other necessary information.


## How it works

General considerations:
- The store owner must have a Payin-Payout store account.
  Sign up [here](https://payin-payout.net/)


## Troubleshooting

- No troubleshooting pending for now.


## Maintainers

- Alexander Kovrigin - [a.kovrigin](https://www.drupal.org/u/akovrigin)
