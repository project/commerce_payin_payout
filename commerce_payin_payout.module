<?php

/**
 * @file
 * Primary module hooks for Commerce Payin-Payout project.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function commerce_payin_payout_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.commerce_payin_payout':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Commerce Payin-Payout integrates Payin-Payout with Drupal Commerce providing payment gateway.') . '</p>';
      $output .= '<h3>' . t('How to use') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('Navigate to Store » Configuration » Payments » Payment Gateways') . '</li>';
      $output .= '<li>' . t('Add new payment gateway and select Payin-Payout plugin.') . '</li>';
      $output .= '<li>' . t('Select payment mode, add API token and other necessary information.') . '</li>';
      $output .= '<li>' . t('Save payment gateway') . '</li>';
      $output .= '</ul>';
      $output .= '<h3>' . t('Features') . '</h3>';
      $output .= '<p>' . t('Since Payin-Payout requires customer phone number to process payment, you will need to create field to store phone numbers (<a href=":profile_types_link">:profile_types_link</a>) and set this field on plugin configuration form.', [':profile_types_link' => '/admin/config/people/profile-types/manage/customer']) . '</p>';
      $output .= '<p>' . t('Visit the <a href=":project_link">Project page</a> on Drupal.org for more information.', [
        ':project_link' => 'https://www.drupal.org/project/commerce_payin_payout',
      ]);

      return $output;
  }
}
